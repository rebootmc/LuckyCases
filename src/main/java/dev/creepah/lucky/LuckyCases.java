package dev.creepah.lucky;

import dev.creepah.lucky.cmd.LuckyCasesCommand;
import dev.creepah.lucky.manager.CasesManager;
import lombok.Getter;
import tech.rayline.core.plugin.RedemptivePlugin;
import tech.rayline.core.plugin.UsesFormats;

@UsesFormats
public final class LuckyCases extends RedemptivePlugin {

    private static LuckyCases instance;
    public static LuckyCases get() {
        return instance;
    }

    @Getter private CasesManager manager;

    @Override
    protected void onModuleEnable() throws Exception {
        instance = this;
        manager = new CasesManager();

        registerCommand(new LuckyCasesCommand());
    }

    @Override
    protected void onModuleDisable() throws Exception {
        manager.save();
    }
}
