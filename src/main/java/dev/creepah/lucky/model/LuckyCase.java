package dev.creepah.lucky.model;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import dev.creepah.lucky.LuckyCases;
import lombok.Data;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.Inventory;
import tech.rayline.core.util.RunnableShorthand;

import java.util.List;
import java.util.Random;

@Data
public final class LuckyCase {

    private Location location;
    private Hologram hologram;
    private boolean active = false;
    private boolean opened = false;
    private boolean superCase;
    private int timer;

    public LuckyCase(Location location, boolean superCase) {
        this.location = location;
        this.superCase = superCase;
        hologram = HologramsAPI.createHologram(LuckyCases.get(), location.clone().add(0.5, 2, 0.5));

        LuckyCases plugin = LuckyCases.get();
        FileConfiguration config = plugin.getConfig();
        List<LuckyItem> items = isSuperCase() ? plugin.getManager().getSuperItems() : plugin.getManager().getItems();

        location.getBlock().setType(Material.CHEST);

        // Randomly fill the chest with case items in random slots.
        Random random = new Random();
        Inventory inv = ((Chest) location.getBlock().getState()).getBlockInventory();
        for (int i = 0; i < random.nextInt(config.getInt("max-items") + 1) + config.getInt("min-items") + 1; i++) {
            int r = random.nextInt(101);
            LuckyItem item = items.get(random.nextInt(items.size()));

            while (item.getChance() >= r) {
                r = random.nextInt(101);
                item = items.get(random.nextInt(items.size()));
            }

            int slot = random.nextInt(inv.getSize());
            while (inv.getItem(slot) != null) slot = random.nextInt(inv.getSize());

            inv.setItem(slot, item.get());
        }

        // Keep updating the hologram until it unlocks.
        timer = config.getInt("unlock-time");
        RunnableShorthand.forPlugin(LuckyCases.get()).with(() -> {
            if (timer > 0) {
                hologram.clearLines();
                hologram.insertTextLine(0, plugin.formatAt("hologram.title").get());
                hologram.insertTextLine(1, plugin.formatAt("hologram.countdown").with("time", timer).get());
            }

            if (timer == 0) {
                setActive(true);
                hologram.clearLines();
                hologram.insertTextLine(0, plugin.formatAt("hologram.title").get());
            }

            timer--;
        }).repeat(20);
    }

    // A method to be called once the case has been opened.
    public void open() {
        LuckyCases plugin = LuckyCases.get();

        setOpened(true);
        plugin.getManager().getActiveCases().remove(this);
        RunnableShorthand.forPlugin(plugin).with(this::clean).later(20 * plugin.getConfig().getInt("disappear", 10));
    }

    // A simple clean-up method.
    public void clean() {
        location.getBlock().setType(Material.AIR);
        hologram.clearLines();
        hologram.delete();
    }
}
