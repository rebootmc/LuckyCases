package dev.creepah.lucky.model;

import lombok.Data;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public final class LuckyItem {

    private Material type;
    private String name;
    private List<String> lore;
    private int amount, data;
    private List<ItemEnchant> enchants;
    private double chance;
    private boolean superItem;

    public LuckyItem(ItemStack stack, double chance, boolean superItem) {
        type = stack.getType();
        amount = stack.getAmount();
        data = stack.getDurability();

        ItemMeta meta = stack.getItemMeta();
        name = meta.getDisplayName();
        lore = meta.getLore();

        enchants = new ArrayList<>();
        enchants.addAll(stack.getEnchantments().keySet().stream()
                .map(e -> new ItemEnchant(e, stack.getEnchantmentLevel(e)))
                .collect(Collectors.toList()));

        this.chance = chance;
        this.superItem = superItem;
    }

    public ItemStack get() {
        ItemStack stack = new ItemStack(type, amount, (short) data);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(name);
        meta.setLore(lore);
        stack.setItemMeta(meta);
        enchants.forEach(e -> e.applyTo(stack));

        return stack;
    }

    @Data
    public static class ItemEnchant {
        private final Enchantment enchant;
        private final int level;

        public ItemStack applyTo(ItemStack stack) {
            stack.addUnsafeEnchantment(enchant, level);
            return stack;
        }
    }
}
