package dev.creepah.lucky.cmd;

import dev.creepah.lucky.LuckyCases;
import dev.creepah.lucky.manager.CasesManager;
import dev.creepah.lucky.model.LuckyItem;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.EmptyHandlerException;
import tech.rayline.core.command.RDCommand;
import tech.rayline.core.gui.ClickAction;
import tech.rayline.core.gui.InventoryGUI;
import tech.rayline.core.gui.InventoryGUIButton;

import java.util.ArrayList;
import java.util.List;

@CommandPermission("luckycases.loot")
public final class LootCommand extends RDCommand {

    public LootCommand() {
        super("loot");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        LuckyCases plugin = LuckyCases.get();
        CasesManager manager = plugin.getManager();
        InventoryGUI gui = new InventoryGUI(plugin, 54, plugin.formatAt("gui.title").get());

        for (LuckyItem item : manager.getAllItems()) {
            ItemStack i = item.get();
            ItemMeta meta = i.getItemMeta();
            List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<>();

            lore.add(" ");
            lore.add(plugin.formatAt("gui.chance").with("chance", item.getChance()).get());
            lore.add(plugin.formatAt("gui.type").with("type", item.isSuperItem() ? "Super" : "Normal").get());

            meta.setLore(lore);
            i.setItemMeta(meta);

            gui.addButton(new InventoryGUIButton(i) {
                @Override
                public void onPlayerClick(Player player, ClickAction action) throws EmptyHandlerException { }
            });
        }

        gui.updateInventory();
        gui.openFor(player);
    }
}
