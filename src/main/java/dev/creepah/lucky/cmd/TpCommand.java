package dev.creepah.lucky.cmd;

import dev.creepah.lucky.LuckyCases;
import dev.creepah.lucky.manager.CasesManager;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.NormalCommandException;
import tech.rayline.core.command.RDCommand;

@CommandPermission("luckycases.tp")
public final class TpCommand extends RDCommand {

    public TpCommand() {
        super("tp");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        int n = 1;
        if (args.length > 0) {
            try {
                n = Integer.parseInt(args[0]);
            } catch (NumberFormatException ex) {
                throw new NormalCommandException("Invalid lucky case specified - please enter a number!");
            }
        }

        CasesManager manager = LuckyCases.get().getManager();
        if (manager.getActiveCases().size() < n) throw new NormalCommandException("Invalid lucky case specified - number too high!");

        Location location = manager.getActiveCases().get(n - 1).getLocation();
        location.getChunk().load();
        player.teleport(location.clone().add(0, 3, 0));
        player.sendMessage(LuckyCases.get().formatAt("teleported").get());
    }
}
