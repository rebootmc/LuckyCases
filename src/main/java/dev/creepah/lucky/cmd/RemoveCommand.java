package dev.creepah.lucky.cmd;

import dev.creepah.lucky.LuckyCases;
import dev.creepah.lucky.manager.CasesManager;
import dev.creepah.lucky.model.LuckyItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.NormalCommandException;
import tech.rayline.core.command.RDCommand;

@CommandPermission("luckycases.remove")
public final class RemoveCommand extends RDCommand {

    public RemoveCommand() {
        super("remove");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        ItemStack hand = player.getItemInHand();
        if (hand == null || hand.getType() == Material.AIR) throw new NormalCommandException("You have no item in your hand!");

        CasesManager manager = LuckyCases.get().getManager();
        LuckyItem item = manager.getItem(hand);
        if (item == null) throw new NormalCommandException("Couldn't find a lucky case item similar to the item in your hand!");

        manager.getAllItems().remove(item);
        player.sendMessage(LuckyCases.get().formatAt("item.removed").get());
    }

}
