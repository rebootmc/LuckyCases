package dev.creepah.lucky.cmd;

import dev.creepah.lucky.LuckyCases;
import org.bukkit.command.CommandSender;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.RDCommand;

@CommandPermission("luckycases.spawn")
public final class SpawnCommand extends RDCommand {

    public SpawnCommand() {
        super("spawn");
    }

    @Override
    protected void handleCommandUnspecific(CommandSender sender, String[] args) throws CommandException {
        LuckyCases.get().getManager().spawnNewLuckyCase();
    }
}
