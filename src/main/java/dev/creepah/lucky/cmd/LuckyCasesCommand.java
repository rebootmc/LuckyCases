package dev.creepah.lucky.cmd;

import dev.creepah.lucky.LuckyCases;
import dev.creepah.lucky.model.LuckyCase;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandMeta;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.RDCommand;

import java.util.List;

@CommandPermission("luckycases.command")
@CommandMeta(description = "Root command for Lucky Cases", aliases = {"cases"})
public final class LuckyCasesCommand extends RDCommand {

    public LuckyCasesCommand() {
        super("lc",
                new AddItemCommand(),
                new SpawnCommand(),
                new TpCommand(),
                new LootCommand(),
                new UpdateCommand(),
                new RemoveCommand());
    }

    @Override
    protected void handleCommandUnspecific(CommandSender sender, String[] args) throws CommandException {
        LuckyCases plugin = LuckyCases.get();
        List<LuckyCase> activeCases = plugin.getManager().getActiveCases();

        sender.sendMessage(plugin.formatAt("messages.list-title").with("count", activeCases.size()).get());

        for (LuckyCase luckyCase : activeCases) {
            Location loc = luckyCase.getLocation();

            sender.sendMessage(plugin.formatAt("messages.list-entry")
                    .with("type", luckyCase.isActive() ? "Unlocked" : "Locked")
                    .with("x", loc.getX()).with("y", loc.getY()).with("z", loc.getZ()).get());
        }
    }
}
