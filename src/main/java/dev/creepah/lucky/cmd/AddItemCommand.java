package dev.creepah.lucky.cmd;

import dev.creepah.lucky.LuckyCases;
import dev.creepah.lucky.model.LuckyItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import tech.rayline.core.command.*;

@CommandPermission("luckycases.additem")
public final class AddItemCommand extends RDCommand {

    public AddItemCommand() {
        super("additem");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        if (args.length == 0) throw new ArgumentRequirementException("Invalid arguments! Usage: /lc additem <chance> [super]");

        double chance;
        try {
            chance = Double.parseDouble(args[0]);
        } catch (NumberFormatException ex) {
            throw new NormalCommandException(args[0] + " is not a valid chance, please enter a number!");
        }

        boolean superItem = (args.length > 1 && args[1].equalsIgnoreCase("Super"));
        ItemStack hand = player.getItemInHand();
        if (hand == null || hand.getType() == Material.AIR) throw new NormalCommandException("You have no item in your hand!");

        LuckyItem item = new LuckyItem(hand, chance, superItem);
        LuckyCases.get().getManager().getAllItems().add(item);

        player.sendMessage(LuckyCases.get().formatAt("item.added").with("chance", chance).with("type", superItem ? "Super" : "Normal").get());
    }
}
