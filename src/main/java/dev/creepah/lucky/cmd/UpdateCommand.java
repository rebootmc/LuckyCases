package dev.creepah.lucky.cmd;

import dev.creepah.lucky.LuckyCases;
import dev.creepah.lucky.manager.CasesManager;
import dev.creepah.lucky.model.LuckyItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import tech.rayline.core.command.*;

@CommandPermission("luckycases.update")
public final class UpdateCommand extends RDCommand {

    public UpdateCommand() {
        super("update");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        if (args.length == 0) throw new ArgumentRequirementException("Invalid arguments! Usage: /lc update <chance>");

        double chance;
        try {
            chance = Double.parseDouble(args[0]);
        } catch (NumberFormatException ex) {
            throw new NormalCommandException(args[0] + " is not a valid chance, please enter a number!");
        }

        ItemStack hand = player.getItemInHand();
        if (hand == null || hand.getType() == Material.AIR) throw new NormalCommandException("You have no item in your hand!");

        CasesManager manager = LuckyCases.get().getManager();
        LuckyItem item = manager.getItem(hand);
        if (item == null) throw new NormalCommandException("Couldn't find a lucky case item similar to the item in your hand!");

        item.setChance(chance);
        player.sendMessage(LuckyCases.get().formatAt("item.updated").with("chance", chance).get());
    }
}
