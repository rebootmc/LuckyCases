package dev.creepah.lucky.manager;

import com.google.gson.Gson;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.massivecore.ps.PS;
import dev.creepah.lucky.LuckyCases;
import dev.creepah.lucky.model.LuckyCase;
import dev.creepah.lucky.model.LuckyItem;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;
import tech.rayline.core.GsonBridge;
import tech.rayline.core.util.RunnableShorthand;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public final class CasesManager {

    private FileConfiguration config = LuckyCases.get().getConfig();
    private Gson GSON = new GsonBridge(LuckyCases.get()).getGson();
    private File file = new File(LuckyCases.get().getDataFolder(), "items.json");

    @Getter private List<LuckyItem> allItems = new ArrayList<>();
    @Getter private List<LuckyCase> activeCases = new ArrayList<>();

    public CasesManager() throws IOException {
        LuckyCases plugin = LuckyCases.get();

        // Spawn a new lucky case every x minutes.
        RunnableShorthand.forPlugin(plugin).with(() -> {
            if (activeCases.size() < config.getInt("max-cases"))
                spawnNewLuckyCase();

        }).delay(20 * 10).repeat(20 * (60 * config.getInt("drop-time")));

        // Load all case allItems from the allItems file.
        if (file.exists()) {
            try (FileReader reader = new FileReader(file)) {
                try {
                    Collections.addAll(allItems, GSON.fromJson(reader, LuckyItem[].class));
                } catch (Exception e) {
                    plugin.getLogger().info("Failed to load case items from file!");
                    e.printStackTrace();
                }
            }
        }

        plugin.observeEvent(InventoryOpenEvent.class)
                .filter(event -> event.getInventory().getHolder() instanceof Chest)
                .subscribe(event -> {
                    LuckyCase luckyCase = getLuckyCase(((Chest) event.getInventory().getHolder()));
                    HumanEntity player = event.getPlayer();

                    if (luckyCase != null) {
                        if (!luckyCase.isActive()) {
                            event.setCancelled(true);
                            player.sendMessage(plugin.formatAt("messages.case-inactive").get());

                        } else {
                            if (!luckyCase.isOpened()) {
                                if (luckyCase.isSuperCase() && !player.hasPermission("luckycases.super")) {
                                    event.setCancelled(true);
                                    player.sendMessage(plugin.formatAt("messages.no-perm").get());
                                    return;
                                }

                                luckyCase.open();
                            }
                        }
                    }
                });

        plugin.observeEvent(BlockBreakEvent.class)
                .filter(event -> event.getBlock().getState() instanceof Chest)
                .subscribe(event -> {
                    LuckyCase luckyCase = getLuckyCase(((Chest) event.getBlock().getState()));

                    if (luckyCase != null) {
                        event.setCancelled(true);
                    }
                });
    }

    // Save all case allItems to the allItems file.
    public void save() throws IOException {
        getActiveCases().forEach(LuckyCase::clean);

        FileWriter writer = new FileWriter(file);
        String data = GSON.toJson(allItems.toArray(new LuckyItem[allItems.size()]));

        writer.write(data);
        writer.flush();
        writer.close();
    }

    // Spawn a new lucky case at a random location and notify the server.
    public void spawnNewLuckyCase() {
        if (allItems.isEmpty()) return;

        int r = ThreadLocalRandom.current().nextInt(100);
        boolean superCase = r < config.getDouble("super-chance", 40);
        LuckyCase luckyCase = new LuckyCase(findAppropriateLocation(), superCase);
        activeCases.add(luckyCase);

        Location loc = luckyCase.getLocation();
        Bukkit.broadcastMessage(LuckyCases.get().formatAt("broadcast")
                .with("type", luckyCase.isSuperCase() ? "Super" : "")
                .with("x", loc.getX()).with("y", loc.getY()).with("z", loc.getZ()).get());
    }

    // Find an appropriate location within the specified range for a lucky case to spawn.
    public Location findAppropriateLocation() {
        int distance = config.getInt("distance", 1000);
        double sX = getSpawn().getX(), sZ = getSpawn().getZ();
        int x = 0, z = 0;
        World w = getSpawn().getWorld();

        while (!isValidLocation(x, z)) {
            x = ThreadLocalRandom.current().nextInt((int) (sX - distance), (int) (sX + distance));
            z = ThreadLocalRandom.current().nextInt((int) (sZ - distance), (int) (sZ + distance));
        }

        return w.getHighestBlockAt(x, z).getLocation();
    }

    // Returns true if the specified x and y coords are a valid place to spawn a case.
    public boolean isValidLocation(int x, int z) {
        if (x == 0 || z ==  0) return false;

        Location loc = getSpawn().getWorld().getHighestBlockAt(x, z).getLocation();
        if (loc.clone().add(0, -1, 0).getBlock().isLiquid()) return false;
        if (BoardColl.get().getFactionAt(PS.valueOf(loc)).isNormal()) return false;
        if (loc.distanceSquared(getSpawn()) < (Math.pow(config.getInt("distance", 1000), 2))) return false;

        return true;
    }

    // Attempt to get a lucky case from the given chest by comparing locations.
    public LuckyCase getLuckyCase(Chest chest) {
        for (LuckyCase c : getActiveCases()) {
            if (c.getLocation().equals(chest.getLocation())) return c;
        }
        return null;
    }

    // Get a lucky case item from the given item stack.
    public LuckyItem getItem(ItemStack stack) {
        for (LuckyItem item : allItems) {
            if (item.get() == stack) return item;
        }

        return null;
    }

    // Get all non-super lucky allItems.
    public List<LuckyItem> getItems() {
        return allItems.stream().filter(i -> !i.isSuperItem()).collect(Collectors.toList());
    }

    // Get all allItems which are super lucky allItems.
    public List<LuckyItem> getSuperItems() {
        return allItems.stream().filter(LuckyItem::isSuperItem).collect(Collectors.toList());
    }

    // Returns a valid spawn location.
    private Location getSpawn() {
        File file = new File(Bukkit.getPluginManager().getPlugin("Essentials").getDataFolder(), "spawn.yml");
        if (file.exists()) {
            FileConfiguration yaml = YamlConfiguration.loadConfiguration(file);

            return new Location(Bukkit.getWorld(yaml.getString("spawns.default.world")),
                    yaml.getDouble("spawns.default.x"),
                    yaml.getDouble("spawns.default.y"),
                    yaml.getDouble("spawns.default.z"));

        } else return Bukkit.getWorlds().get(0).getSpawnLocation();
    }
}
